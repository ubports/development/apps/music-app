#include <libintl.h>
#include <QGuiApplication>
#include "config.h"
#include "musicplayer.h"

int main (int argc, char** argv)
{
    unsetenv ("QML_FIXED_ANIMATION_STEP");

    MusicPlayer application (argc, argv);

    textdomain("lomiri-music-app");
    std::string localeDir = localeDirectory().toStdString();
    bindtextdomain("lomiri-music-app", localeDir.c_str());
    bind_textdomain_codeset("lomiri-music-app", "UTF-8");

    if (!application.setup ())
    {
        return 1;
    }

    return application.exec ();
}
